<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210121203344 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE application (id VARCHAR(255) NOT NULL, applicant_id VARCHAR(255) NOT NULL, full_name BOOLEAN DEFAULT NULL, company_name BOOLEAN DEFAULT NULL, company_siret BOOLEAN DEFAULT NULL, company_localisation BOOLEAN DEFAULT NULL, comment TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A45BDDC197139001 ON application (applicant_id)');
        $this->addSql('CREATE TABLE application_user (application_id VARCHAR(255) NOT NULL, user_id VARCHAR(255) NOT NULL, PRIMARY KEY(application_id, user_id))');
        $this->addSql('CREATE INDEX IDX_7A7FBEC13E030ACD ON application_user (application_id)');
        $this->addSql('CREATE INDEX IDX_7A7FBEC1A76ED395 ON application_user (user_id)');
        $this->addSql('ALTER TABLE application ADD CONSTRAINT FK_A45BDDC197139001 FOREIGN KEY (applicant_id) REFERENCES "User" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE application_user ADD CONSTRAINT FK_7A7FBEC13E030ACD FOREIGN KEY (application_id) REFERENCES application (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE application_user ADD CONSTRAINT FK_7A7FBEC1A76ED395 FOREIGN KEY (user_id) REFERENCES "User" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE application_user DROP CONSTRAINT FK_7A7FBEC13E030ACD');
        $this->addSql('DROP TABLE application');
        $this->addSql('DROP TABLE application_user');
    }
}
