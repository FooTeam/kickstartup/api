<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210119164525 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "User" ADD firstname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "User" ADD lastname VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "User" ADD response_notification BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE "User" ADD apply_notification BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE "User" ADD donation_notification BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE "User" ALTER status DROP DEFAULT');
        $this->addSql('ALTER TABLE project ALTER status DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE project ALTER status SET DEFAULT 0');
        $this->addSql('ALTER TABLE "User" DROP firstname');
        $this->addSql('ALTER TABLE "User" DROP lastname');
        $this->addSql('ALTER TABLE "User" DROP response_notification');
        $this->addSql('ALTER TABLE "User" DROP apply_notification');
        $this->addSql('ALTER TABLE "User" DROP donation_notification');
        $this->addSql('ALTER TABLE "User" ALTER status DROP DEFAULT');
    }
}
