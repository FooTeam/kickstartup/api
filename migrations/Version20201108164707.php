<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201108164707 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Project & Media tables with relations (included those with User).';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE media (id VARCHAR(255) NOT NULL, project_id VARCHAR(255) DEFAULT NULL, url VARCHAR(255) NOT NULL, alt VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6A2CA10C166D1F9C ON media (project_id)');
        $this->addSql('CREATE TABLE project (id VARCHAR(255) NOT NULL, owner_id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, status INT NOT NULL DEFAULT 0, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE7E3C61F9 ON project (owner_id)');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "User" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "User" ADD avatar_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "User" ADD CONSTRAINT FK_2DA1797786383B10 FOREIGN KEY (avatar_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_2DA1797786383B10 ON "User" (avatar_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "User" DROP CONSTRAINT FK_2DA1797786383B10');
        $this->addSql('ALTER TABLE media DROP CONSTRAINT FK_6A2CA10C166D1F9C');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP INDEX IDX_2DA1797786383B10');
        $this->addSql('ALTER TABLE "User" DROP avatar_id');
    }
}
