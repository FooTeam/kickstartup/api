<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    /**
     * @Route("/users/me/{username}", name="app_lucky_number")
     * @param string $username
     * @param UserRepository $userRepo
     * @return Response
     */
    public function number(string $username, UserRepository $userRepo): Response
    {
        $user = $userRepo->findOneBy([
            'email' => $username
        ]);

        return $this->json(['me' => $user]);
    }
}
