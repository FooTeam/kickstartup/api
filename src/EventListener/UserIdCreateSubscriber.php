<?php

namespace App\EventListener;

use App\Entity\User;
use App\Entity\Media;
use App\Entity\Project;
use App\Entity\Company;
use App\Entity\Application;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Uid\Uuid;

class UserIdCreateSubscriber implements EventSubscriber
{

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof User && !$entity instanceof Media && !$entity instanceof Project && !$entity instanceof Company && !$entity instanceof Application ) {
            return;
        }
        $entity->setId(Uuid::v4());
    }

}
