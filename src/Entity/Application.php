<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ApplicationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ApplicationRepository::class)
 */
class Application
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="application", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $applicant;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="review")
     * @ORM\JoinColumn(nullable=true)
     */
    private $reviewer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fullName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $companySiret;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $companyLocalisation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    public function __construct()
    {
        $this->reviewer = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getApplicant(): ?User
    {
        return $this->applicant;
    }

    public function setApplicant(User $applicant): self
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getReviewer(): Collection
    {
        return $this->reviewer;
    }

    public function addReviewer(User $reviewer): self
    {
        if (!$this->reviewer->contains($reviewer)) {
            $this->reviewer[] = $reviewer;
        }

        return $this;
    }

    public function removeReviewer(User $reviewer): self
    {
        if ($this->reviewer->contains($reviewer)) {
            $this->reviewer->removeElement($reviewer);
        }

        return $this;
    }

    public function getFullName(): ?bool
    {
        return $this->fullName;
    }

    public function setFullName(?bool $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getCompanyName(): ?bool
    {
        return $this->companyName;
    }

    public function setCompanyName(?bool $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getCompanySiret(): ?bool
    {
        return $this->companySiret;
    }

    public function setCompanySiret(?bool $companySiret): self
    {
        $this->companySiret = $companySiret;

        return $this;
    }

    public function getCompanyLocalisation(): ?bool
    {
        return $this->companyLocalisation;
    }

    public function setCompanyLocalisation(?bool $companyLocalisation): self
    {
        $this->companyLocalisation = $companyLocalisation;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
